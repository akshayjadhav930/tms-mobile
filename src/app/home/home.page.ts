import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../services/http.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  desiganation: any;
  userId:any;
  regionList: any = [];
  selectedOption: any;
  dashbordResponse: any = [];
  dashbordResponse2: any = [];
  id_RegionLocal:any;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private _http: HttpService
  ) {}

  ionViewWillEnter() {
    this.desiganation = localStorage.getItem('id_designation');
    this.userId= localStorage.getItem('id_user');
    this.id_RegionLocal = localStorage.getItem('id_region');
    this.selectedOption= this.id_RegionLocal;
    this.getRegionList();
   
    this.getDashboardData(this.id_RegionLocal);
  }

  getRegionList() {
    this._http.get_by_observable('/getRegionList').subscribe((result) => {
      console.log('Regions :=', result);
      this.regionList = result;
      console.log('Regions2 :=', this.regionList);
    });
  }

  afterSelect(e: any) {
    let idRegion = e.detail.value;
    console.log('e', e);
    this.getDashboardData(idRegion);
  }

  getDashboardData(region: any) {
    const params = {
      id_region: region,
      id_user: this.userId,
      id_designation: this.desiganation,
      id_customer: 0
    }
    this._http.post_by_observable('/getDashboardDataApp', params).subscribe((result) => {
        console.log('dashBoardAfterSelect :', result);
        this.dashbordResponse = result;
        if(this.desiganation==3) {
        this.dashbordResponse = this.dashbordResponse.filter((updatedRes:any)=> updatedRes.id_status!=="2");
        console.log("upRes",this.dashbordResponse);
      }
      });
  }

  logOut() {
    localStorage.clear();
    this.router.navigate(['login']);
  }

  onClick() {
    this.router.navigate(['sr-list']);
  }

  cardClick(col:any) {

     console.log("responseSate",col);

     this.router.navigate(['sr-list', col]);
    
      }
}
