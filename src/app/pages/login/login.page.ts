import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from "src/app/services/http.service";
import { ToastrService } from 'ngx-toastr';
import { ToastController } from '@ionic/angular';
import { CommonserviceService } from 'src/app/services/commonservice.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm:FormGroup;
  submitted =false;

  constructor(private _http:HttpService,
              private commserv:CommonserviceService,
              private formBuilder:FormBuilder,
              private _router:Router,
              private _toastr:ToastrService,
              private _itoast:ToastController
    ) { 
      this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(4)]],
      })

    }

  ngOnInit() {
    
  }
  get f() { return this.loginForm.controls; }

  onSubmit() {

    
      this.submitted=true;
      
    if (this.loginForm.valid){
        const params = {
          "username": this.f['username'].value,
          "password": this.f['password'].value
        }
         this._http.post_by_observable("/appLogin",params).subscribe((result)=>{
           console.log("login",result)

           localStorage.setItem("id_designation",result.userData[0].id_designation)
           localStorage.setItem("id_user",result.userData[0].id_user)
           localStorage.setItem("id_region",result.userData[0].id_region)
           localStorage.setItem("name",result.userData[0].name)
           localStorage.setItem("username",result.userData[0].username)
           localStorage.setItem("isLogin", "1");
           
           if(result.userData[0].id_designation==2) {
            this.commserv.showToast("Invalid user","danger");
            localStorage.clear();
           } else{
            this._router.navigate(["home"]);
            this.commserv.showToast("Login successful!","success");
           //this._toastr.success("Login successful!","Success");
           }

         },
         (err)=>{
          //this._toastr.error("invalid credentials!","Error")   
          this.commserv.showToast("invalid credentials!","danger")
         }
         )
        }       
    }    
}
