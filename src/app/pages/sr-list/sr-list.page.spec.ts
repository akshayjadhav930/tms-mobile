import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SrListPage } from './sr-list.page';

describe('SrListPage', () => {
  let component: SrListPage;
  let fixture: ComponentFixture<SrListPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(SrListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
