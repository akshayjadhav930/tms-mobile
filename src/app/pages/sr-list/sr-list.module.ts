import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SrListPageRoutingModule } from './sr-list-routing.module';

import { SrListPage } from './sr-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SrListPageRoutingModule
  ],
  declarations: [SrListPage]
})
export class SrListPageModule {}
