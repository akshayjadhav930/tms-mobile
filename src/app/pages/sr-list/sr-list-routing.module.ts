import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SrListPage } from './sr-list.page';

const routes: Routes = [
  {
    path: '',
    component: SrListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SrListPageRoutingModule {}
