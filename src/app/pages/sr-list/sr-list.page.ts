import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-sr-list',
  templateUrl: './sr-list.page.html',
  styleUrls: ['./sr-list.page.scss'],
})
export class SrListPage implements OnInit {
  params: any = [];
  id_status: any = 0;
  status:any;
  constructor(
    private _http: HttpService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private modalController:ModalController
  ) {
    this.activeRoute.params.subscribe((params:any) => {
      this.id_status = params.id_status;
      this.status=params.status;
    
      console.log('this.id_status', this.status);
    });
  }

  ngOnInit() {}

  logOut() {
    localStorage.clear();
    this.router.navigate(['login']);
  }

  onClick() {
    this.router.navigate(['home']);
  }

  // closeDrawer() {
  //   this.modalController.dismiss();
  // }
}
