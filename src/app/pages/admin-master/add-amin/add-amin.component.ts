import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from "src/app/services/http.service";
import { ModalController, ToastController } from '@ionic/angular';
import { CommonserviceService } from 'src/app/services/commonservice.service';


@Component({
  selector: 'app-add-amin',
  templateUrl: './add-amin.component.html',
  styleUrls: ['./add-amin.component.scss'],
})
export class AddAminComponent  implements OnInit {
 addAdminForm:FormGroup;
 submitted =false;
 regionList:any=[];
 region_id:any;
 selectedOption:any;
 data:any;
 name:any;
 mobile:any;
 email:any;
 address:any;
 region:any;
 username:any;
 userId:any;

  constructor(private router:Router,
              private _http:HttpService,
              private _comnserv:CommonserviceService,
              private formBuilder: FormBuilder,
              private _itoast:ToastController,
              private modalControl:ModalController) { 

                this.addAdminForm=this.formBuilder.group({

                  name: ['',Validators.required],
                  id_region :['',Validators.required],
                  username :['',Validators.required],
                  password :['',[Validators.required]],
                  mobile_no : ['',Validators.required],
                  email : ['',Validators.required],
                  address :['',Validators.required],
                  is_active:['',Validators.required],

                })
              }
  
  ionViewWillEnter() {
    this.selectedOption="0";
    this.getRegionList();
    console.log("data",this.data);
    // this.name=this.adminList[0].name
  } 

  ngOnInit() {}

  get addf() {
    return this.addAdminForm.controls;
  }

  getRegionList() {
    this._http.get_by_observable('/getRegionList').subscribe((result) => {
      // console.log('Regions :=', result);
      this.regionList = result;
      console.log('Regions2 :=', this.regionList);
    });
  }
  // afterSelect(e:any) {
  //  this.region_id =e.detail.value;
  //  console.log("region_id",this.region_id);
  // }

  async closeModal() {
   await this.modalControl.dismiss();
  }

  updateAdmin() {
    this.submitted=true;
    const params={
      "id_user":this.userId,
      "password": this.addf['password'].value,
    }
    if(this.addAdminForm.valid){

      this._http.post_by_observable('/updatePassword',params).subscribe((result)=>{
        console.log("updated user and password :",result);

        if(result.error==1) {
          this._comnserv.showToast(result.message,"danger");
        }else{
          this.closeModal();
          this._comnserv.showToast(result.message, "success");
        } 
      })
    }
    
  }

  onSubmit() {
    this.submitted=true;
    if(this.addAdminForm.valid){
      const params = {
        "name": this.addf['name'].value,
        "id_region": this.addf['id_region'].value,
        "username": this.addf['username'].value,
        "password": this.addf['password'].value,
        "mobile_no": this.addf['mobile_no'].value,
        "email": this.addf['email'].value,
        "address": this.addf['address'].value,
        "id_designation": "2",
        "is_active": "1",
        "created_at" : new Date()     
    }

    console.log("Params",params);

    this._http.post_by_observable('/createAdminByApp',params).subscribe((result)=>{

      if(result.error==1) {
        this._comnserv.showToast(result.message,"danger");
      }else{
        this.closeModal();
        this._comnserv.showToast(result.message,"success");
       
      } 
     })

    }
  }
}


