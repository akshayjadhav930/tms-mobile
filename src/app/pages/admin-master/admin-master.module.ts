import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminMasterPageRoutingModule } from './admin-master-routing.module';

import { AdminMasterPage } from './admin-master.page';
import { AddAminComponent } from './add-amin/add-amin.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminMasterPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AdminMasterPage, AddAminComponent]
})
export class AdminMasterPageModule {}
