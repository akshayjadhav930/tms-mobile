import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AddAminComponent } from './add-amin/add-amin.component';
import { HttpService } from 'src/app/services/http.service';
@Component({
  selector: 'app-admin-master',
  templateUrl: './admin-master.page.html',
  styleUrls: ['./admin-master.page.scss'],
})
export class AdminMasterPage implements OnInit {
  adminList:any=[];

  constructor( private router:Router,
              private modalControl:ModalController,
              private _http:HttpService) { }

  ionViewWillEnter() {
    this.getAdminList();
  }            

  ngOnInit() {
  }

     async addAdmin(data: any = {}, col:any = {}) {
      const modal = await this.modalControl.create({
        component: AddAminComponent,
        componentProps: {
          data:data,
          name:col.name,
          mobile:col.mobile_no,
          email:col.email,
          address:col.address,
          region:col.id_region,
          username:col.username,
          userId:col.id_user,
        },
        // breakpoints: [0.75],
        // initialBreakpoint: 0.75,
      });
      modal.present();
    }

    getAdminList() {
      this._http.get_by_observable('/adminListApi').subscribe((result)=>{
        this.adminList=result;
        console.log("getAdminlist :",result);
        // console.log("adminList2 :",this.adminList2);
      })
    }


}
