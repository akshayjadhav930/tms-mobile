import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { SrListPage } from './pages/sr-list/sr-list.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  constructor(private modalController: ModalController,
              private menuController: MenuController,
              private router: Router) {}
  
    // async openBottomDrawer() {
    //   const modal = await this.modalController.create({
    //     component: SrListPage,
    //     cssClass: 'bottom-drawer'
    //   });
  
    //   await modal.present();
    // }

    openPage(page: string) {
      // Close the menu
      this.menuController.close();
  
      // Navigate to the selected page
      this.router.navigate([`/${page}`]);
    }
}
