import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { SignupGuard } from './guards/signup.guard';
import { AuthGuard } from './guards/auth.guard';
import { AddAminComponent } from './pages/admin-master/add-amin/add-amin.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
    canActivate:[AuthGuard]
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule),
    canActivate:[SignupGuard]
  },
  {
    path: 'sr-list',
    loadChildren: () => import('./pages/sr-list/sr-list.module').then( m => m.SrListPageModule),
    canActivate:[AuthGuard]

  },
  {
    path: 'admin-master',
    loadChildren: () => import('./pages/admin-master/admin-master.module').then( m => m.AdminMasterPageModule),
    canActivate:[AuthGuard]
  },
  // {
  //   path: 'add-admin',
  //   component: AddAminComponent,
  //   canActivate:[AuthGuard]
  // },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
