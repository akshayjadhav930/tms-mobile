import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { from, Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class HttpService {
  username = "MSpatil";
  password = "Patil@123";
  public httpHeader = {
    "Content-Type": "application/json",
  };
  isNative = false;
  constructor(private _http: HttpClient) {}

  post_by_observable(serviceName: string, data: any): Observable<any> {
    const url = environment.apiURL + serviceName;
    let headers = {
      "Content-Type": "application/json",
     // Empcode: "1578",
      Authorization:
        "Basic " + window.btoa(this.username + ":" + this.password),
    };
    return this._http.post(url, JSON.stringify(data), { headers });
  }

  delete_by_observable(serviceName: string, data: any): Observable<any> {
    const url = environment.apiURL + serviceName;
    let headers = {
      "Content-Type": "application/json",
      Empcode: 1578,
      Authorization:
        "Basic " + window.btoa(this.username + ":" + this.password),
    };
    return this._http.delete(url);
  }

  put_by_observable(serviceName: string, data: any): Observable<any> {
    const url = environment.apiURL + serviceName;
    let headers = {
      "Content-Type": "application/json",
      Authorization:
        "Basic " + window.btoa(this.username + ":" + this.password),
    };
    return this._http.put(url, data);
  }

  get_by_observable(url: string = "",pageNo = 1,pageSize = 10
  
   ): Observable<any> {
    const params = new HttpParams().set("Empcode", 1578);

    const httpOptions = {
      headers: new HttpHeaders({
        Accept: "application/json",
        Empcode: "1578",
        pageNo: "" + pageNo,
        pageSize: "" + pageSize,
        Authorization:
          "Basic " + window.btoa(this.username + ":" + this.password),
      }),
    };

    // const customHeaders = new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   'Empcode': '1578',
    //   'Authorization': 'Basic '+window.btoa(this.username + ':' + this.password)
    // });

    // const httpOptions = {
    //   headers: customHeaders
    // }
    return this._http.get(environment.apiURL + url, httpOptions);
  }

  get_by_observable2(url: string = ""): Observable<any> {
    const headers = { "Content-Type": "application/json" };
    return this._http.get(url, { headers });
  }
}
