import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CommonserviceService {

  constructor(private _itoast:ToastController) { }

  showToast(err:any,col:any) {
    const myToast = this._itoast.create({
      message : err,
      color: col,
      duration : 2000
    }).then((result)=>{
      console.log(result);
      result.present();
    })
  }

}
