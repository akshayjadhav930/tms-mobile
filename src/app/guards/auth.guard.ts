import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate,Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate() : Promise<boolean>{
    return new Promise( resolve => {
      let login = localStorage.getItem('isLogin') == '1' ? true : false ;
      if(login){
        resolve(true);
      }else{
        this.router.navigate(['login']);
        resolve(false);
      }
    });
  } 
  
}
