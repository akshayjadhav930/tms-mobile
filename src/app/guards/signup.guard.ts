import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate,Router ,RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignupGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(): Promise<boolean> {
    return new Promise((resolve) => {
      let login = localStorage.getItem("isLogin") == "1" ? true : false;
      if (login) {
        this.router.navigate(["home"]);
        resolve(false);
      } else {
        resolve(true);
      }
    });
  }
  
}
